let { todoCollection } = require("../model/todos.model")

const fetchAllTodos = (req, res) => {
    return res.json(todoCollection);
}

const createTodo = (req, res) => {
    const { label } = req.body;
    if (req.body && label) {
        let newItem = {
            id: "t00" + (todoCollection.length + 1),
            label,
            status: "pending"
        }
        todoCollection = [newItem, ...todoCollection];
        return res.send(newItem);
    }
    return res.send({ message: "Request Body/Label Not Found" })
}

const fetchTodoById = (req, res) => {
    const { todoId } = req.params;
    const foundItem = todoCollection.find(todo => todo.id === todoId)
    if (foundItem) {
        return res.send(foundItem)
    }
    return res.send({ message: "Item not found for ID - " + todoId })
}

const deleteTodoById = (req, res) => {
    const { todoId } = req.params;
    const position = todoCollection.findIndex(todo => todo.id === todoId)
    if (position >= 0) {
        const deleteElements = todoCollection.splice(position, 1)
        return res.send(deleteElements[0])
    }
    return res.send({ message: "Unable to delete for id - " + todoId })
}

const updateTodoById = (req, res) => {
    const { todoId } = req.params;
    const position = todoCollection.findIndex(todo => todo.id === todoId)
    if (position >= 0) {
        todoCollection[position] = { ...todoCollection[position], ...req.body };
        return res.send(todoCollection[position]);
    }
    return res.send({ message: "Unable to update for ID - " + todoId })
}

module.exports = {
    fetchAllTodos,
    createTodo,
    fetchTodoById,
    deleteTodoById,
    updateTodoById
}