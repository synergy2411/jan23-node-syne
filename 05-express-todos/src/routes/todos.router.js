const express = require("express");
const TodoRouter = express.Router();
const { fetchAllTodos, createTodo, fetchTodoById, deleteTodoById, updateTodoById } = require("../controller/todos.controller");


TodoRouter.route('')            // /api/todos       
    .get(fetchAllTodos)
    .post(createTodo)

TodoRouter.route("/:todoId")        // /api/todos/:todoId
    .get(fetchTodoById)
    .delete(deleteTodoById)
    .patch(updateTodoById)


module.exports = TodoRouter;

