const express = require("express");
const TodoRouter = require("./routes/todos.router");

const app = express();

app.use(express.json())

app.use("/api/todos", TodoRouter)

app.listen(9090, () => console.log("Serving Todo endpoints..."))