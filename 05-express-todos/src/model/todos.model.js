let todoCollection = [
    { id: "t001", label: "grocery", status: "completed" },
    { id: "t002", label: "shopping", status: "pending" },
    { id: "t003", label: "insurance", status: "completed" },
    { id: "t004", label: "planting", status: "pending" },
]

module.exports = { todoCollection }