const chatterName = prompt("Please enter username...")

const btnSend = document.getElementById("btn-send");
const inputMessage = document.getElementById("txtMsg");
const taInput = document.getElementById("taMsg");

const socket = io.connect("http://localhost:9090");
socket.on("acknowledge", (data) => {
    console.log(data);
});

socket.on("ServerMessage", data => {
    const { chatterName, message } = data;
    taInput.append(chatterName + " : " + message + "\n")
})

btnSend.addEventListener("click", (event) => {
    event.preventDefault();
    socket.emit("ClientMessage", { chatterName, message: inputMessage.value })
    inputMessage.value = '';
})
