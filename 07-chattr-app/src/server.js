const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app)
const io = require("socket.io")(server);

app.use(express.static(__dirname + '/public'));

io.on("connection", client => {
    console.log("Client Connected");
    client.emit("acknowledge", { message: "Connected now!!" })
    client.on("ClientMessage", data => {
        const { chatterName, message } = data;
        console.log(chatterName + " Says : ", message);
        client.emit("ServerMessage", { chatterName: "Me", message });
        client.broadcast.emit("ServerMessage", { chatterName, message });
    })
})

app.get("/", (_, res) => res.sendFile(__dirname + "/public/index.html"))

server.listen(9090, () => console.log("Socket server startet at PORT : 9090"))
