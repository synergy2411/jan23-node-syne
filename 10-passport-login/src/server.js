const express = require("express");
const session = require("express-session");
const MongoStore = require("connect-mongo");
const passport = require("passport");
require("ejs")
require("./db")
require("dotenv").config();
const { initializePassportConfig } = require("./config/passport.config")

const UserRouter = require("./routes/users.routes");

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const { mongoUser, mongoPassword } = process.env;
const PORT = process.env.PORT || 9000;

// const mongoUrl = `mongodb://localhost:27017/my-db`;
const mongoUrl = `mongodb+srv://${mongoUser}:${mongoPassword}@cluster0.e9xsq.mongodb.net/my-db`;

app.use(session({
    secret: "My Secret for Session",
    resave: false,
    saveUninitialized: true,
    store: MongoStore.create({ mongoUrl: mongoUrl, collectionName: "sessions" })
}))

app.use(passport.initialize());
app.use(passport.session())
// Configuration for Passport
initializePassportConfig(passport)

app.set("view engine", "ejs");
app.set("views", "./src/views")

app.use("/users", UserRouter);


app.listen(PORT, () => console.log("Server started at PORT : ", PORT))