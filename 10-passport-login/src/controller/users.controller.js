const UserModel = require("../model/users.model")

const getRegistrationForm = (req, res) => {
    res.render("register")
}

const getLoginForm = (req, res) => {
    res.render("login")
}

const userLogin = (req, res) => {
    console.log(req.body)
    return res.send({ message: "LOGIN - SUCCESS" })
}

const userRegistration = async (req, res) => {
    try {
        const newUser = new UserModel(req.body)
        const savedUser = await newUser.save();
        res.redirect("/users/login")
    } catch (err) {
        console.log(err);
        return res.send(err)
    }
}

const userProfile = (req, res) => res.render("profile");
const userLogout = (req, res) => {
    console.log("[AUTHENTICATED]", req.isAuthenticated());
    req.logout(() => {
        console.log("[AUTHENTICATED]", req.isAuthenticated());
        res.redirect("/users/login")
    });
}
module.exports = {
    getRegistrationForm,
    getLoginForm,
    userLogin,
    userRegistration,
    userProfile,
    userLogout
}