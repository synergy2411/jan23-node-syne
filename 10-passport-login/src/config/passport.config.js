const UserModel = require("../model/users.model");

const LocalStrategy = require("passport-local").Strategy;

const initializePassportConfig = passport => {
    passport.use(new LocalStrategy(async function (username, password, done) {
        try {
            const foundUser = await UserModel.findOne({ username, password })
            if (!foundUser) {
                done(null, false)
            }
            done(null, foundUser)
        } catch (err) {
            done(err)
        }
    }))

    // to maintain the user data in passport session
    passport.serializeUser((user, done) => {
        return done(null, { id: user.id })
    })
    passport.deserializeUser((user, done) => {
        return done(null, user)
    })
}

module.exports = {
    initializePassportConfig
}