const UserRouter = require("express").Router();
const passport = require("passport");

const { getRegistrationForm, getLoginForm, userRegistration, userLogout, userProfile } = require("../controller/users.controller")

UserRouter.route("/signup")            // http://localhost:9090/users/signup
    .get(getRegistrationForm)


UserRouter.route("/login")            // http://localhost:9090/users/login
    .get(getLoginForm)
    .post(passport.authenticate("local", {
        successRedirect: "/users/profile",
        failureRedirect: "/users/login"
    }))

UserRouter.route("/profile")
    .get(userProfile)

UserRouter.route("/registration")            // http://localhost:9090/users/login
    .post(userRegistration)

UserRouter.route("/logout")
    .get(userLogout)

module.exports = UserRouter;