const express = require("express");
require("ejs");

const app = express();

app.use(express.static(__dirname + "/public"))

app.set("view engine", "ejs")

const friends = ["Joe", "Monica", "Ross", "Rachel"]
// const friends = []


app.get("/", (req, res) => {
    res.render("home", {
        username: "Foo Bar",
        age: 32,
        friends
    })
})

app.get("/about", (req, res) => res.render("about"))

app.listen(9001, () => console.log("Server started at PORT: 9001"))