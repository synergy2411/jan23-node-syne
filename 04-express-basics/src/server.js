const express = require('express');

const app = express()

app.use(express.urlencoded({ extended: false }))             // parsing the req body

app.post("/user/login", (req, res) => {

    if (req.body) {
        console.log(req.body);
        return res.send({ message: "SUCCESS" })
    }
    return res.send({ message: "ERROR" })

})

app.get("/user/login", (req, res) => {
    if (req.query) {
        console.log(req.query)
        return res.send({ message: "SUCCESS" }).status(200)
    }
    return res.send({ message: "ERROR" })
})

app.get("/add-user", (req, resp) => {
    resp.sendFile(__dirname + "/public/add-user.html")
})

app.get("/", (request, response, next) => {
    console.log("METHOD : ", request.method)
    console.log("[HEADERS]", request.headers)
    response.send({ message: "SUCEESS" }).status(200)
})

app.listen(9001, () => console.log("Express server started..."))