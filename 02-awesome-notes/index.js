const yargs = require("yargs");
const { addNote, readNote, removeNote, listNotes } = require("./utils/notes");

// Create 'add' command
yargs.command({
    command: "add",
    description: "to add new note",
    builder: {
        title: {
            type: "string",
            demandOption: true
        },
        body: {
            type: "string",
            demandOption: true
        }
    },
    handler: (args) => {
        const { title, body } = args;
        addNote(title, body);
    }
})

// 'read' command
yargs.command({
    command: "read",
    description: "to read one note",
    builder: {
        title: {
            type: "string",
            demandOption: true
        }
    },
    handler: args => {
        readNote(args.title)
    }
})

// 'remove' command
yargs.command({
    command: "remove",
    description: "to remove one note",
    builder: {
        title: {
            type: "string",
            demandOption: true
        }
    },
    handler: args => {
        removeNote(args.title)
    }
})

// 'list' command
yargs.command({
    command: "list",
    description: "to list down all notes",
    handler: () => {
        listNotes()
    }
})

yargs.parse();


// node index.js --help
// node index.js add --help

// > node index.js add --title="New Title" --body="New Title Body"
// > node index.js read --title="Some Title"
// > node index.js remove --title="Some Title"
// > node index.js list
