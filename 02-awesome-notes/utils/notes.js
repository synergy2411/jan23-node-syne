const fs = require("fs");
require("colors");

const loadNotes = () => {
    try {
        const bufferContent = fs.readFileSync("data.json");
        const strContent = bufferContent.toString();
        return JSON.parse(strContent)
    } catch (err) {
        return [];
    }
}

const saveNotes = (notes) => {
    fs.writeFileSync("data.json", JSON.stringify(notes))
    console.log("Notes Stored".green)
}

const addNote = (title, body) => {
    const allNotes = loadNotes();
    const foundNote = allNotes.filter(note => note.title.toUpperCase() === title.toUpperCase())
    if (foundNote.length > 0) {
        return console.log("Title already exist. Try Again!!".red)
    }
    const newNote = { title, body };
    allNotes.push(newNote);
    saveNotes(allNotes);
}

const readNote = title => {
    const allNotes = loadNotes();
    const foundNotes = allNotes.filter(n => n.title.toUpperCase() === title.toUpperCase())
    if (foundNotes.length <= 0) {
        return console.log(`Title NOT found - ${title}`.red)
    }
    console.log(`Title : ${foundNotes[0].title}`.inverse)
    console.log(`Body : ${foundNotes[0].body}`.inverse)
}
const removeNote = (title) => {
    const allNotes = loadNotes();
    const position = allNotes.findIndex(n => n.title.toUpperCase() === title.toUpperCase())
    if (position >= 0) {
        allNotes.splice(position, 1)
        return saveNotes(allNotes);
    }
    console.log(`Title NOT found - ${title}`.red)
}
const listNotes = () => {
    const allNotes = loadNotes();
    allNotes.forEach(note => {
        console.log("_________________".grey);
        console.log(`Title : ${note.title}`.blue);
        console.log(`Body : ${note.body}`.blue);
    });
}

module.exports = {
    addNote,
    readNote,
    removeNote,
    listNotes
}