- Tea Break : 11:00 (15Minutes)
- Lunch Break : 01:00 (45 Minutes)
- Tea Break : 03:30 (15 Minutes)

# What is JavaScript?

- dynamic web application / mobile app / hybrid app (Ionic / React Native)
- Asynchronous language
- client-side (Browser) as well as Server-side (NRE) programming language
- Single threaded
- Loosely typed
- light-weight
- event driven
- non-blocking
- interpreted as well as compiled

## Asynchronous Task (will be handled by different thread)

- Make remote server calls - 1.5s
- timers
- reading / writing data
- obtaining the sockets

# MEAN / MERN -> JavaScript (Non-blocking Single Thread)

VBScript

## JavaScript engine executes all Sync tasks first and then executes Async task

# NodeJS Installer

- Node Runtime Environment (NRE) : executes JavaScript Code
- Node Package Manager (NPM) : to manage the project dependencies
- Node Native Modules (eg. http, events, fs, os, path etc)

# to generate package.json

- npm init -y

# NodeJS Module System

- CommonJS Module (Default) : file extension .js

  > import - require()
  > export - module.exports variable

- ES6 Module / ESM : file extension .mjs
  > import statement
  > export statement

# Sementic Versioning (X.Y.Z)

- X : Major Version : add new functionality in app which is NOT backward compatible
- Y : Minor Version : add new functionality in App which is backward compatible
- Z : Patch Version : fixing the errors, patches for improving code/ error messages etc

  3.4.8 => 3.4.9

  3.4.8 => 4.0.0

  3.4.8 => 3.5.0

# to publish project on NPM registry

- npm adduser
- npm publish

# REST Services / Endpoints

- GET /api/todos : to fetch all items
- GET /api/todos/{id} : to fetch single item
- POST /api/todos + Request Body : to create an item
- DELETE /api/todos/{id} : to delete an item
- PATCH /api/todos/{id} + Request Body : to update an item

# Express

- req.params : to access to Route Parameters
- req.query : to access queryStrings

# View Engines : implements Server-Side-Rendering (SSR)

- jade/pug : whitespaces and indentations
- handlebars : {{ }}
- ejs : <% %>
- vash : @model

# Socket Programming : socket.io

# NodeJS Security

- API Protection
- JWT
- PassportJS

# MongoDB

MongoDB Atlas SRV : mongodb+srv://synergy:ctfhJKphSD3jV5J7@cluster0.e9xsq.mongodb.net/test
MongoURL : mongodb://localhost:27017

- Create Free Cluster available
- Database access - Add New User
- Network Access - Add your current IP
