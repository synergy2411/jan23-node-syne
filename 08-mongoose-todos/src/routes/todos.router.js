const { fetchAllTodos, createTodo, fetchTodo, deleteTodoById, updateTodo } = require("../controller/todos.controller");

const express = require("express");

const TodoRouter = express.Router();

TodoRouter.route("/")
    .get(fetchAllTodos)
    .post(createTodo)
TodoRouter.route("/:todoId")
    .get(fetchTodo)
    .delete(deleteTodoById)
    .patch(updateTodo)

module.exports = TodoRouter;