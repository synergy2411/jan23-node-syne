const TodoModel = require("../model/todos.model");

const fetchAllTodos = async (_, res) => {
    try {
        const allTodos = await TodoModel.find()
        return res.send(allTodos);
    } catch (err) {
        console.log(err);
        return res.send(err)
    }
}

const createTodo = async (req, res) => {
    try {
        const newTodo = new TodoModel(req.body)
        const savedItem = await newTodo.save()
        return res.send(savedItem)
    } catch (err) {
        console.log(err);
        return res.send(err)
    }
}

const fetchTodo = async (req, res) => {
    try {
        const { todoId } = req.params;
        const result = await TodoModel.findById(todoId)
        if (result) {
            return res.send(result)
        }
        return res.send({ message: "Unable to find record for ID -" + todoId })
    } catch (err) {
        console.log(err);
        return res.send(err)
    }
}

const deleteTodoById = async (req, res) => {
    try {
        const { todoId } = req.params;
        const result = await TodoModel.findByIdAndDelete(todoId)
        return res.send(result)
    } catch (err) {
        console.log(err);
        return res.sen(err);
    }
}

const updateTodo = async (req, res) => {
    try {
        const { todoId } = req.params;
        const result = await TodoModel.findByIdAndUpdate(todoId, req.body)
        return res.send(result)
    } catch (err) {
        console.log(err);
        return res.sen(err);
    }
}
module.exports = {
    fetchAllTodos,
    createTodo,
    fetchTodo,
    deleteTodoById,
    updateTodo
}