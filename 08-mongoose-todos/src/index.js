const express = require("express");
require("./db")
require("dotenv").config()
const TodoRouter = require("./routes/todos.router");

const app = express();
const PORT = process.env.PORT || 9000;

app.use(express.json());

app.use("/api/todos", TodoRouter)

app.listen(PORT, () => console.log("Server running on PORT ", PORT))