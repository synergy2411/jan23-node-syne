const { Schema, model } = require("mongoose");

const todoSchema = new Schema({
    label: {
        type: Schema.Types.String,
        required: [true, "Label is mandatory field"],
        validate: {
            validator: (val) => {
                return val.length >= 4
            },
            message: (prop) => `${prop.value} should have at least 4 characters`
        }
    },
    status: {
        type: Schema.Types.String,
        default: "pending"
    }
}, { versionKey: false })

// Business Hooks
todoSchema.pre("save", function (next) {
    console.log("[PRE SAVE]");
    next()
})

todoSchema.post("save", function () {
    console.log("[POST SAVE]");
})

const TodoModel = model("Todo", todoSchema)

module.exports = TodoModel;


// Todo => todos
// Book => books
