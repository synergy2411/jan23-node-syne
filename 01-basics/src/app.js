const { randomNumber } = require("sk-syne-random-number");

console.log("Generated Number is : ", randomNumber(100))



// console.log("Start")            // Sync

// setTimeout(() => {              // Async
//     console.log("3 seconds left")
// }, 0)

// // MicroTasks
// Promise.resolve("RESOLVED").then(response => console.log(response))         // Async

// console.log("End")              // Sync