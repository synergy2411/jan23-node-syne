const EventEmitter = require("events").EventEmitter;

const emitter = new EventEmitter();

// on() : run the listener function / subscribing event
// emit() : emits / produce / trigger the event


const handlerFn = (data) => {
    console.log("Handler Function executed with status : ", data.result)
}

emitter.on("message", handlerFn)

emitter.emit("message", { result: "SUCCESS" })          // custom event

// Streams API
// Process Object
// Request/Response

// btn.addEventListener("click", () => {})


process.on("exit", () => {
    console.log("Existing the process...")
})
// process.exit(0);

process.on("uncaughtException", () => {
    console.log("Exception occured")
})

nonExistingFn();


