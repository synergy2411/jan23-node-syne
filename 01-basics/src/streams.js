const Stream = require("stream").Stream;
const EventEmitter = require("events").EventEmitter;
const fs = require("fs");


const readStream = fs.createReadStream("./test.md")
const writeStream = fs.createWriteStream("./newtest.md")
const writeStreamTwo = fs.createWriteStream("./newtestTwo.md")

readStream.on("data", (chunk) => {
    writeStream.write(chunk)
})

readStream.pipe(writeStreamTwo)



// console.log(new Stream.Readable() instanceof EventEmitter)
// console.log(new Stream.Writable() instanceof EventEmitter)
// console.log(new Stream.Transform() instanceof EventEmitter)
// console.log(new Stream.Duplex() instanceof EventEmitter)