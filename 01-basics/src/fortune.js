const LUCKY_NUMBER = Math.floor(Math.random() * 100);

const getDailyQuote = () => "Run 5 miles today!";

module.exports = {
    getLuckyNumber: () => LUCKY_NUMBER,
    getDailyQuote           // Short hand object property
}