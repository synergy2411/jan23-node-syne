"use strict";

var _require = require("sk-syne-random-number"),
  randomNumber = _require.randomNumber;
console.log("Generated Number is : ", randomNumber(100));

// console.log("Start")            // Sync

// setTimeout(() => {              // Async
//     console.log("3 seconds left")
// }, 0)

// // MicroTasks
// Promise.resolve("RESOLVED").then(response => console.log(response))         // Async

// console.log("End")              // Sync