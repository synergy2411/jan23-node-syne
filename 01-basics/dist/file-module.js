"use strict";

var _require = require("./fortune"),
  getLuckyNumber = _require.getLuckyNumber,
  getDailyQuote = _require.getDailyQuote;
var _require2 = require("./utils"),
  sum = _require2.sum;
console.log("Your Lucky Number Today : ", getLuckyNumber());
console.log("Your Daily Quote : ", getDailyQuote());
console.log("Sum is : ", sum(2, 4));