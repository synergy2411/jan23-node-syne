"use strict";

// const path = require("path")

// let url = "http://www.example.com/public/index.html";

// console.log("Directory Name : ", path.dirname(url));

// console.log("File Name : ", path.basename(url));

// console.log("Extension : ", path.extname(url));

// const os = require("os");

// console.log("Total Memory : ", os.totalmem())

// console.log("Free Memory : ", os.freemem());

// console.log("Architecture : ", os.arch())

// console.log("Number of CPU's : ", os.cpus().length)

// const fs = require("fs");

// const data = fs.readFileSync("./app.js")
// console.log("Sync Data : ", data.toString())

// fs.readFile("./app.js", (err, result) => {
//     if (err) {
//         return console.log(err)
//     }
//     console.log("RESULT : ", result.toString())
// })

var http = require("http");
var server = http.createServer(function (request, response) {
  response.writeHead(200);
  response.write("Hello Again!!");
  response.end();
});
server.listen(9090, function () {
  console.log("Server Started at PORT : 9090");
});