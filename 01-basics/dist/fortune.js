"use strict";

var LUCKY_NUMBER = Math.floor(Math.random() * 100);
var getDailyQuote = function getDailyQuote() {
  return "Run 5 miles today!";
};
module.exports = {
  getLuckyNumber: function getLuckyNumber() {
    return LUCKY_NUMBER;
  },
  getDailyQuote: getDailyQuote // Short hand object property
};