const { connect } = require("mongoose");
require("dotenv").config()

const { mongoUser, mongoPassword } = process.env;

// const mongoURI = `mongodb://localhost:27017`;
const mongoURI = `mongodb+srv://${mongoUser}:${mongoPassword}@cluster0.e9xsq.mongodb.net/my-db`

connect(mongoURI)
    .then(conn => console.log("Mongo Connected!"))
    .catch(err => console.error(err))