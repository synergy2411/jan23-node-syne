const { Schema, model } = require("mongoose")

const userSchema = new Schema({
    username: {
        type: Schema.Types.String,
        required: [true, "Username required"]
    },
    password: {
        type: Schema.Types.String,
        required: [true, "Password required"]
    }
})

const UserModel = model("User", userSchema);

module.exports = UserModel;