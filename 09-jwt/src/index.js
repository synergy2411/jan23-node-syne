const express = require("express");
const { sign, verify } = require("jsonwebtoken");
require("./db");
const UserModel = require("./model/users.model");
require("dotenv").config();


const app = express()

app.use(express.json());

const { SECRET_KEY } = process.env;

app.post("/user/register", async (req, res) => {
    try {
        const { username, password } = req.body;
        const newUser = new UserModel(req.body);
        const savedUser = await newUser.save();
        return res.send(savedUser);
    } catch (err) {
        console.log(err);
        return res.send(err)
    }


})

app.post("/user/login", async (req, res) => {
    try {
        const { username, password } = req.body;
        const foundUser = await UserModel.findOne({ username, password })
        if (foundUser) {
            const token = sign({ id: foundUser._id }, SECRET_KEY, { expiresIn: "12h" })
            return res.send({ token, message: "Logged In" })
        }
        return res.send({ message: "Bad Credentials" })
    } catch (err) {
        console.log(err);
        return res.send(err)
    }
})

app.get("/api/public", (req, res) => {
    res.send({ message: "public api" })
})

const ensureToken = (req, res, next) => {
    const authHeader = req.headers.authorization
    if (!authHeader) {
        return res.send({ error: "Authorization required" })
    }
    const token = authHeader.split(" ")[1]            // ["Bearer", "token_Value"]
    req.token = token;
    next();
}

app.get("/api/protected", ensureToken, async (req, res) => {
    try {
        const decode = verify(req.token, SECRET_KEY)
        const user = await UserModel.findById(decode.id)
        if (user) {
            return res.send({ message: `Hello ${user.username}, You are accessing protected API` })
        }
        return res.send({ message: "Unable to locate user" })
    } catch (err) {
        console.log(err);
        return res.send(err)
    }
})

app.listen(9090, () => console.log("Server started at PORT 9090"))